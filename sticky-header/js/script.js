


const header = document.querySelector('.header')
const toogleClass = 'sticky'

window.addEventListener('scroll',()=>{
    const currentScroll = window.pageYOffset;
    if(currentScroll > 150){
        header.classList.add(toogleClass)
    }else{
        header.classList.remove(toogleClass)
    }
})

const navEl = document.querySelector('.nav')
const hamburger = document.querySelector('.hamburger')

hamburger.addEventListener('click', ()=> {
    navEl.classList.toggle('nav--open')
    hamburger.classList.toggle('menu--open')
})
navEl.addEventListener('click', ()=> {
    navEl.classList.remove('nav--open')
    hamburger.classList.remove('menu--open')
})                                                                                                                                                                             